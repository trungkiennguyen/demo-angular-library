import { OnInit } from '@angular/core';
export declare class NgCButtonComponent implements OnInit {
    buttonName: string;
    hero: string;
    isShowModal: Boolean;
    constructor();
    ngOnInit(): void;
    showHideModal(): void;
}

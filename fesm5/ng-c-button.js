import { Injectable, NgModule, Component, Input, defineInjectable } from '@angular/core';

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
var NgCButtonService = /** @class */ (function () {
    function NgCButtonService() {
    }
    NgCButtonService.decorators = [
        { type: Injectable, args: [{
                    providedIn: 'root'
                },] }
    ];
    /** @nocollapse */
    NgCButtonService.ctorParameters = function () { return []; };
    /** @nocollapse */ NgCButtonService.ngInjectableDef = defineInjectable({ factory: function NgCButtonService_Factory() { return new NgCButtonService(); }, token: NgCButtonService, providedIn: "root" });
    return NgCButtonService;
}());

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
var NgCButtonComponent = /** @class */ (function () {
    function NgCButtonComponent() {
        this.buttonName = 'test';
        this.hero = 'FinOS';
        this.isShowModal = false;
    }
    /**
     * @return {?}
     */
    NgCButtonComponent.prototype.ngOnInit = /**
     * @return {?}
     */
    function () {
    };
    /**
     * @return {?}
     */
    NgCButtonComponent.prototype.showHideModal = /**
     * @return {?}
     */
    function () {
        this.isShowModal = !this.isShowModal;
    };
    NgCButtonComponent.decorators = [
        { type: Component, args: [{
                    selector: 'ng-c-button',
                    template: "<button class=\"c-button\" #btnNgPay id=\"btnNgPay\" (click)=\"showHideModal()\">{{buttonName}}</button>\n\n<div id=\"myModal\" class=\"c-modal\" [hidden]=\"!isShowModal\">\n  <div class=\"c-modal-content\">\n    <span class=\"c-close\" (click)=\"showHideModal()\">&times;</span>\n    <p>My hero: {{hero}}</p>\n  </div>\n\n</div>",
                    styles: [".c-button{background-color:#4caf50;border:none;color:#fff;padding:15px 32px;text-align:center;text-decoration:none;display:inline-block;font-size:16px}.c-modal{position:fixed;z-index:1;padding-top:100px;left:0;top:0;width:100%;height:100%;overflow:auto;background-color:rgba(0,0,0,.4)}.c-modal-content{background-color:#fefefe;margin:auto;padding:20px;border:1px solid #888;width:80%}.c-close{color:#aaa;float:right;font-size:28px;font-weight:700}.c-close:focus,.c-close:hover{color:#000;text-decoration:none;cursor:pointer}"]
                }] }
    ];
    /** @nocollapse */
    NgCButtonComponent.ctorParameters = function () { return []; };
    NgCButtonComponent.propDecorators = {
        buttonName: [{ type: Input }],
        hero: [{ type: Input }]
    };
    return NgCButtonComponent;
}());

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
var NgCButtonModule = /** @class */ (function () {
    function NgCButtonModule() {
    }
    NgCButtonModule.decorators = [
        { type: NgModule, args: [{
                    declarations: [NgCButtonComponent],
                    imports: [],
                    exports: [NgCButtonComponent]
                },] }
    ];
    return NgCButtonModule;
}());

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */

export { NgCButtonService, NgCButtonComponent, NgCButtonModule };

//# sourceMappingURL=ng-c-button.js.map
/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component, Input } from '@angular/core';
export class NgCButtonComponent {
    constructor() {
        this.buttonName = 'test';
        this.hero = 'FinOS';
        this.isShowModal = false;
    }
    /**
     * @return {?}
     */
    ngOnInit() {
    }
    /**
     * @return {?}
     */
    showHideModal() {
        this.isShowModal = !this.isShowModal;
    }
}
NgCButtonComponent.decorators = [
    { type: Component, args: [{
                selector: 'ng-c-button',
                template: "<button class=\"c-button\" #btnNgPay id=\"btnNgPay\" (click)=\"showHideModal()\">{{buttonName}}</button>\n\n<div id=\"myModal\" class=\"c-modal\" [hidden]=\"!isShowModal\">\n  <div class=\"c-modal-content\">\n    <span class=\"c-close\" (click)=\"showHideModal()\">&times;</span>\n    <p>My hero: {{hero}}</p>\n  </div>\n\n</div>",
                styles: [".c-button{background-color:#4caf50;border:none;color:#fff;padding:15px 32px;text-align:center;text-decoration:none;display:inline-block;font-size:16px}.c-modal{position:fixed;z-index:1;padding-top:100px;left:0;top:0;width:100%;height:100%;overflow:auto;background-color:rgba(0,0,0,.4)}.c-modal-content{background-color:#fefefe;margin:auto;padding:20px;border:1px solid #888;width:80%}.c-close{color:#aaa;float:right;font-size:28px;font-weight:700}.c-close:focus,.c-close:hover{color:#000;text-decoration:none;cursor:pointer}"]
            }] }
];
/** @nocollapse */
NgCButtonComponent.ctorParameters = () => [];
NgCButtonComponent.propDecorators = {
    buttonName: [{ type: Input }],
    hero: [{ type: Input }]
};
if (false) {
    /** @type {?} */
    NgCButtonComponent.prototype.buttonName;
    /** @type {?} */
    NgCButtonComponent.prototype.hero;
    /** @type {?} */
    NgCButtonComponent.prototype.isShowModal;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibmctYy1idXR0b24uY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vbmctYy1idXR0b24vIiwic291cmNlcyI6WyJsaWIvbmctYy1idXR0b24uY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFVLEtBQUssRUFBRyxNQUFNLGVBQWUsQ0FBQztBQVExRCxNQUFNLE9BQU8sa0JBQWtCO0lBSzdCO1FBSlMsZUFBVSxHQUFXLE1BQU0sQ0FBQztRQUM1QixTQUFJLEdBQVcsT0FBTyxDQUFDO1FBQ2hDLGdCQUFXLEdBQVksS0FBSyxDQUFDO0lBRWIsQ0FBQzs7OztJQUVqQixRQUFRO0lBQ1IsQ0FBQzs7OztJQUVELGFBQWE7UUFDWCxJQUFJLENBQUMsV0FBVyxHQUFHLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQztJQUN2QyxDQUFDOzs7WUFqQkYsU0FBUyxTQUFDO2dCQUNULFFBQVEsRUFBRSxhQUFhO2dCQUN2QixxVkFBMkM7O2FBRTVDOzs7Ozt5QkFFRSxLQUFLO21CQUNMLEtBQUs7Ozs7SUFETix3Q0FBcUM7O0lBQ3JDLGtDQUFnQzs7SUFDaEMseUNBQTZCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBPbkluaXQsIElucHV0ICB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuXG5cbkBDb21wb25lbnQoe1xuICBzZWxlY3RvcjogJ25nLWMtYnV0dG9uJyxcbiAgdGVtcGxhdGVVcmw6ICcuL25nLWMtYnV0dG9uLmNvbXBvbmVudC5odG1sJyxcbiAgc3R5bGVVcmxzOiBbJy4vbmctYy1idXR0b24uY29tcG9uZW50LnNjc3MnXVxufSlcbmV4cG9ydCBjbGFzcyBOZ0NCdXR0b25Db21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xuICBASW5wdXQoKSBidXR0b25OYW1lOiBzdHJpbmcgPSAndGVzdCc7XG4gIEBJbnB1dCgpIGhlcm86IHN0cmluZyA9ICdGaW5PUyc7XG4gIGlzU2hvd01vZGFsOiBCb29sZWFuID0gZmFsc2U7XG5cbiAgY29uc3RydWN0b3IoKSB7IH1cblxuICBuZ09uSW5pdCgpIHtcbiAgfVxuXG4gIHNob3dIaWRlTW9kYWwoKSB7XG4gICAgdGhpcy5pc1Nob3dNb2RhbCA9ICF0aGlzLmlzU2hvd01vZGFsO1xuICB9XG5cbn1cbiJdfQ==
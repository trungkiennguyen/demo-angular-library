/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*
 * Public API Surface of ng-c-button
 */
export { NgCButtonService } from './lib/ng-c-button.service';
export { NgCButtonComponent } from './lib/ng-c-button.component';
export { NgCButtonModule } from './lib/ng-c-button.module';
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicHVibGljX2FwaS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL25nLWMtYnV0dG9uLyIsInNvdXJjZXMiOlsicHVibGljX2FwaS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7O0FBSUEsaUNBQWMsMkJBQTJCLENBQUM7QUFDMUMsbUNBQWMsNkJBQTZCLENBQUM7QUFDNUMsZ0NBQWMsMEJBQTBCLENBQUMiLCJzb3VyY2VzQ29udGVudCI6WyIvKlxuICogUHVibGljIEFQSSBTdXJmYWNlIG9mIG5nLWMtYnV0dG9uXG4gKi9cblxuZXhwb3J0ICogZnJvbSAnLi9saWIvbmctYy1idXR0b24uc2VydmljZSc7XG5leHBvcnQgKiBmcm9tICcuL2xpYi9uZy1jLWJ1dHRvbi5jb21wb25lbnQnO1xuZXhwb3J0ICogZnJvbSAnLi9saWIvbmctYy1idXR0b24ubW9kdWxlJztcbiJdfQ==
/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { NgModule } from '@angular/core';
import { NgCButtonComponent } from './ng-c-button.component';
var NgCButtonModule = /** @class */ (function () {
    function NgCButtonModule() {
    }
    NgCButtonModule.decorators = [
        { type: NgModule, args: [{
                    declarations: [NgCButtonComponent],
                    imports: [],
                    exports: [NgCButtonComponent]
                },] }
    ];
    return NgCButtonModule;
}());
export { NgCButtonModule };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibmctYy1idXR0b24ubW9kdWxlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vbmctYy1idXR0b24vIiwic291cmNlcyI6WyJsaWIvbmctYy1idXR0b24ubW9kdWxlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUUsUUFBUSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQ3pDLE9BQU8sRUFBRSxrQkFBa0IsRUFBRSxNQUFNLHlCQUF5QixDQUFDO0FBRTdEO0lBQUE7SUFNK0IsQ0FBQzs7Z0JBTi9CLFFBQVEsU0FBQztvQkFDUixZQUFZLEVBQUUsQ0FBQyxrQkFBa0IsQ0FBQztvQkFDbEMsT0FBTyxFQUFFLEVBQ1I7b0JBQ0QsT0FBTyxFQUFFLENBQUMsa0JBQWtCLENBQUM7aUJBQzlCOztJQUM4QixzQkFBQztDQUFBLEFBTmhDLElBTWdDO1NBQW5CLGVBQWUiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBOZ01vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgTmdDQnV0dG9uQ29tcG9uZW50IH0gZnJvbSAnLi9uZy1jLWJ1dHRvbi5jb21wb25lbnQnO1xuXG5ATmdNb2R1bGUoe1xuICBkZWNsYXJhdGlvbnM6IFtOZ0NCdXR0b25Db21wb25lbnRdLFxuICBpbXBvcnRzOiBbXG4gIF0sXG4gIGV4cG9ydHM6IFtOZ0NCdXR0b25Db21wb25lbnRdXG59KVxuZXhwb3J0IGNsYXNzIE5nQ0J1dHRvbk1vZHVsZSB7IH1cbiJdfQ==
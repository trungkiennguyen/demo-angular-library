/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component, Input } from '@angular/core';
var NgCButtonComponent = /** @class */ (function () {
    function NgCButtonComponent() {
        this.buttonName = 'test';
        this.hero = 'FinOS';
        this.isShowModal = false;
    }
    /**
     * @return {?}
     */
    NgCButtonComponent.prototype.ngOnInit = /**
     * @return {?}
     */
    function () {
    };
    /**
     * @return {?}
     */
    NgCButtonComponent.prototype.showHideModal = /**
     * @return {?}
     */
    function () {
        this.isShowModal = !this.isShowModal;
    };
    NgCButtonComponent.decorators = [
        { type: Component, args: [{
                    selector: 'ng-c-button',
                    template: "<button class=\"c-button\" #btnNgPay id=\"btnNgPay\" (click)=\"showHideModal()\">{{buttonName}}</button>\n\n<div id=\"myModal\" class=\"c-modal\" [hidden]=\"!isShowModal\">\n  <div class=\"c-modal-content\">\n    <span class=\"c-close\" (click)=\"showHideModal()\">&times;</span>\n    <p>My hero: {{hero}}</p>\n  </div>\n\n</div>",
                    styles: [".c-button{background-color:#4caf50;border:none;color:#fff;padding:15px 32px;text-align:center;text-decoration:none;display:inline-block;font-size:16px}.c-modal{position:fixed;z-index:1;padding-top:100px;left:0;top:0;width:100%;height:100%;overflow:auto;background-color:rgba(0,0,0,.4)}.c-modal-content{background-color:#fefefe;margin:auto;padding:20px;border:1px solid #888;width:80%}.c-close{color:#aaa;float:right;font-size:28px;font-weight:700}.c-close:focus,.c-close:hover{color:#000;text-decoration:none;cursor:pointer}"]
                }] }
    ];
    /** @nocollapse */
    NgCButtonComponent.ctorParameters = function () { return []; };
    NgCButtonComponent.propDecorators = {
        buttonName: [{ type: Input }],
        hero: [{ type: Input }]
    };
    return NgCButtonComponent;
}());
export { NgCButtonComponent };
if (false) {
    /** @type {?} */
    NgCButtonComponent.prototype.buttonName;
    /** @type {?} */
    NgCButtonComponent.prototype.hero;
    /** @type {?} */
    NgCButtonComponent.prototype.isShowModal;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibmctYy1idXR0b24uY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vbmctYy1idXR0b24vIiwic291cmNlcyI6WyJsaWIvbmctYy1idXR0b24uY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFVLEtBQUssRUFBRyxNQUFNLGVBQWUsQ0FBQztBQUcxRDtJQVVFO1FBSlMsZUFBVSxHQUFXLE1BQU0sQ0FBQztRQUM1QixTQUFJLEdBQVcsT0FBTyxDQUFDO1FBQ2hDLGdCQUFXLEdBQVksS0FBSyxDQUFDO0lBRWIsQ0FBQzs7OztJQUVqQixxQ0FBUTs7O0lBQVI7SUFDQSxDQUFDOzs7O0lBRUQsMENBQWE7OztJQUFiO1FBQ0UsSUFBSSxDQUFDLFdBQVcsR0FBRyxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUM7SUFDdkMsQ0FBQzs7Z0JBakJGLFNBQVMsU0FBQztvQkFDVCxRQUFRLEVBQUUsYUFBYTtvQkFDdkIscVZBQTJDOztpQkFFNUM7Ozs7OzZCQUVFLEtBQUs7dUJBQ0wsS0FBSzs7SUFZUix5QkFBQztDQUFBLEFBbkJELElBbUJDO1NBZFksa0JBQWtCOzs7SUFDN0Isd0NBQXFDOztJQUNyQyxrQ0FBZ0M7O0lBQ2hDLHlDQUE2QiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0LCBJbnB1dCAgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcblxuXG5AQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6ICduZy1jLWJ1dHRvbicsXG4gIHRlbXBsYXRlVXJsOiAnLi9uZy1jLWJ1dHRvbi5jb21wb25lbnQuaHRtbCcsXG4gIHN0eWxlVXJsczogWycuL25nLWMtYnV0dG9uLmNvbXBvbmVudC5zY3NzJ11cbn0pXG5leHBvcnQgY2xhc3MgTmdDQnV0dG9uQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcbiAgQElucHV0KCkgYnV0dG9uTmFtZTogc3RyaW5nID0gJ3Rlc3QnO1xuICBASW5wdXQoKSBoZXJvOiBzdHJpbmcgPSAnRmluT1MnO1xuICBpc1Nob3dNb2RhbDogQm9vbGVhbiA9IGZhbHNlO1xuXG4gIGNvbnN0cnVjdG9yKCkgeyB9XG5cbiAgbmdPbkluaXQoKSB7XG4gIH1cblxuICBzaG93SGlkZU1vZGFsKCkge1xuICAgIHRoaXMuaXNTaG93TW9kYWwgPSAhdGhpcy5pc1Nob3dNb2RhbDtcbiAgfVxuXG59XG4iXX0=